/**
 * @file
 * Script for the mediaflow image/video selector
 */
(function ($) {
  Drupal.behaviors.mediaFlowEmbedd = {
    attach: function (context) {
      $('[data-mediaflow-id]').each(function () {
        var placeholder = $(this);
        var id = placeholder.data('mediaflow-id');
        $.get('/mediaflow/embedd_media/' + id, function (data) {
          placeholder.replaceWith(data.markup);
        });
      });
    }
  }
})(jQuery);
