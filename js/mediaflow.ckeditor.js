(function (Drupal, CKEDITOR, $) {
  CKEDITOR.plugins.add('mediaflow', {
    requires: 'drupalmedia',
    icons: 'mediaflow',
    hidpi: false,
    beforeInit: function beforeInit(editor) {
      editor.addCommand('mediaflow', {
        allowedContent: {
          'drupal-media': {
            attributes: {
              '!data-entity-type': true,
              '!data-entity-uuid': true,
              '!data-view-mode': true,
              '!data-align': true,
              '!data-caption': true,
              '!alt': true,
              '!title': true
            },
            classes: {}
          }
        },
        requiredContent: new CKEDITOR.style({
          element: 'drupal-media',
          attributes: {
            'data-entity-type': '',
            'data-entity-uuid': ''
          }
        }),
        modes: {
          wysiwyg: 1
        },
        canUndo: true,
        exec: function exec(editor) {
          var settings = {};
          var fileSelector;
          var backDrop = $('<div />')
            .addClass('mediaflow-ckeditor-backdrop');

          var modal = $('<div />')
            .attr('id', 'mediaflow-ckeditor-modal')
            .addClass('mediaflow-ckeditor-modal');

          var selector = $('<div />')
            .attr('id', 'mediaflow-ckeditor-browser')
            .addClass('mediaflow-ckeditor-browser')
            .appendTo(modal);

          backDrop.click(function () {
            backDrop.detach();
            modal.detach();
          });
          console.log(editor.config.mediaflow);
          $.extend(settings, editor.config.mediaflow);
          $.extend(settings, {
            success: function(response) {
              editor.fire('saveSnapshot');
              console.log(settings);
              $.get({
                url: '/mediaflow/add_media',
                data: {
                  'mediaflow-info': JSON.stringify(response),
                  'media-type': settings.ckeditor_type,
                },
                success: function (response) {
                  var mediaElement = editor.document.createElement('drupal-media');
                  Object.keys(response).forEach(function (key) {
                    mediaElement.setAttribute(key, response[key]);
                  });
                  editor.insertHtml(mediaElement.getOuterHtml());
                  editor.fire('saveSnapshot');
                  backDrop.detach();
                  modal.detach();
                },
              });
            },
          });
          $('body').append(backDrop).append(modal);
          fileSelector = new FileSelector(selector.attr('id'), settings);
        }
      });
      if (editor.ui.addButton) {
        editor.ui.addButton('Mediaflow', {
          label: Drupal.t('Mediaflow'),
          command: 'mediaflow'
        });
      }
    },
    data: function (event) {
    }
  });
})(Drupal, CKEDITOR, jQuery);
