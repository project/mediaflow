/**
 * @file
 * Script for the mediaflow image/video selector
 */

 function isJson(text) {
  if(/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
  replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
  replace(/(?:^|:|,)(?:\s*\[)+/g, ''))){
    return true;
  }

  return false;
}

(function ($) {
  "use strict";
 Drupal.behaviors.mediaFlowPicker = {
  attach: function (context) {
    if($(context).find('.mediaflow-widget-wrapper-file').length > 0) {
      $('.mediaflow-widget-wrapper-file input').each(function(){
        if(isNaN($(this).val())){
          var jsonVal = JSON.parse($(this).val());
          $(this).parents('.mediaflow-widget-wrapper-file').find('.mediaflow-file').show();
          $(this).parents('.mediaflow-widget-wrapper-file').find('.mediaflow-file').html('<img class="image-style-large" src="'+jsonVal.url+'" />');
        }
      });

      $('.remove-mediaflow-button').on('click', function(){
        $(this).parents('.mediaflow-widget-wrapper-file').find("[data-mediaflow-target]").val(0);

        $(this).parents('.mediaflow-widget-wrapper-file').find("img").remove();
        $(this).parents('.mediaflow-widget-wrapper-file').siblings("img").remove();

        $(this).parents('.mediaflow-widget-wrapper-file').find(".plyr").remove();
        $(this).parents('.mediaflow-widget-wrapper-file').siblings(".plyr").remove();

        $(this).parents('.mediaflow-widget-wrapper-file').find(".mediaflow-video").remove();
        $(this).parents('.mediaflow-widget-wrapper-file').siblings(".mediaflow-video").remove();

        $(this).parents('.mediaflow-widget-wrapper-file').find('.remove-mediaflow-button').hide();
      });
      $('.pick-mediaflow-button').once().on('click', function(){
        var $this = $(this),
        $preview = $this.parents('.mediaflow-widget-wrapper-file').find(".mediaflow-file"),
        $input = $this.parents('.mediaflow-widget-wrapper-file').find('[data-mediaflow-target]');

        var settings = {};
        $.extend(settings, drupalSettings.mediaflow);
        $.extend(settings, {
          success: function(response) {
            $preview.show();
            $preview.html('');
            $this.parents('.mediaflow-widget-wrapper-file').find("img").remove();
            $this.parents('.mediaflow-widget-wrapper-file').siblings("img").remove();

            if(response.basetype == "video"){
              var mediaElement = document.createElement('div');
              mediaElement.classList = "mediaflow-video";
              mediaElement.innerHTML = response.embedCode;
              $preview.append(mediaElement);
            } else{
              var mediaElement = document.createElement('img');
              mediaElement.setAttribute("src", response.url);
              $preview.append(mediaElement);
            }

            $input
            .val(JSON.stringify(response))
            .trigger('change');

            backDrop.detach();
            modal.detach();
          },
        });

        var fileSelector;
        var backDrop = $('<div />')
          .addClass('mediaflow-ckeditor-backdrop');

        var modal = $('<div />')
          .attr('id', 'mediaflow-ckeditor-modal')
          .addClass('mediaflow-ckeditor-modal');

        var selector = $('<div />')
          .attr('id', 'mediaflow-ckeditor-browser')
          .addClass('mediaflow-ckeditor-browser')
          .appendTo(modal);

        backDrop.click(function () {
          backDrop.detach();
          modal.detach();
        });

        $('body').append(backDrop).append(modal);
        fileSelector = new FileSelector(selector.attr('id'), settings);

      });
    }
  },
  detach: function (context) {
    if($(context).find('.mediaflow-widget-wrapper-file').length > 0) {
      $('.remove-mediaflow-button').off('click');
    }
  }

  };

  $.fn.mediaFlowBrowser = function () {
    this.each(function () {
      var settings = {};
      var container = $(this);
      var fileSelector;

      $.extend(settings, drupalSettings.mediaflow);
      $.extend(settings, {
        success: function(response) {
          $('[data-mediaflow-target]')
            .val(JSON.stringify(response))
            .trigger('change');
          $('[data-drupal-selector="edit-submit"]').click();
        },
      });
      fileSelector = new FileSelector(container.attr('id'), settings);
      $('[data-drupal-selector="edit-submit"]').hide();
    });
  }

  Drupal.behaviors.mediaFlowSelector = {
    attach: function (context) {
      var editor = document.querySelectorAll(".mediaflow-file");
      $('[data-mediaflow-selector]', context).once('mediaflow').mediaFlowBrowser();
    }
  }

})(jQuery);
