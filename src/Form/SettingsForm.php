<?php
namespace Drupal\mediaflow\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\media\Entity\MediaType;
use Drupal\mediaflow\Plugin\media\Source\Mediaflow;

class SettingsForm extends ConfigFormBase {

  const METHODS = [
    'choose' => 'Choose in media selector',
    'iframe' => 'Iframe',
    'js' => 'Javascript',
  ];

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['mediaflow.settings'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'mediaflow_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mediaflow.settings');

    $form = parent::buildForm($form, $form_state);

    $form['refresh_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MFP Key'),
      '#default_value' => $config->get('refresh_token'),
    ];

 /*   $form['allow_crop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow cropping'),
      '#default_value' => $config->get('allow_crop'),
    ];*/

    $form['set_alt_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enforce-Alt texts'),
      '#default_value' => $config->get('set_alt_text'),
    ];

    $form['ckeditor_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type to use in ckeditor.'),
      '#default_value' => $config->get('ckeditor_type'),
      '#options' => $this->mediaTypeOptions(),
    ];

    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Video embed method.'),
      '#default_value' => $config->get('method'),
      '#options' => $this->methodOptions(),
    ];

    /** @var \Drupal\mediaflow\Service\MediaflowFetcher $fetcher */
    $fetcher = Drupal::service('mediaflow.fetcher');
    $status = $fetcher->renewAccessToken();

    if ($status) {
      $info = '<p>' . $this->t('Your connection to Mediaflow works.') . '</p>';
      $info .= '<p>' . $this->t('Your next steps may include:') . '</p>';
      $info .= '<ul><li>';
      $info .= $this->t('Adding Mediaflow button to your ckeditor toolbar for each text format:') . ' ';
      $link = Link::createFromRoute($this->t('Text formats'),'filter.admin_overview');
      $info .= $link->toString() . '</li>';

      $info .= '<li>' . $this->t('Adding Media fields to your node types:') . ' ';

      $link = Link::createFromRoute($this->t('Node types'),'entity.node_type.collection');
      $info .= $link->toString() . '</li>';

      /** @var MediaType $type */
      $type = MediaType::load('mediaflow');
      if ($type) {
        $info .= '<li>' . $this->t('Configuring your Mediaflow Media Type:') . ' ';
        $link = $type->toLink($this->t('Mediaflow media type'), 'edit-form');
        $info .= $link->toString() . '</li>';
      }

      if (Drupal::moduleHandler()->moduleExists('entity_browser')) {
        $info .= '<li>' . $this->t('Set up an Entity Browser with a Mediaflow widget:') . ' ';
        $link = Link::createFromRoute($this->t('Entity Browsers'),'entity.entity_browser.collection');
        $info .= $link->toString() . '</li>';
      }

      $info .= '</ul>';

      $form['info'] = [
        '#type' =>  'item',
        '#title' => $this->t('Status'),
        '#markup' => $info,
        '#weight' => 100,
      ];
    }
    else if (!empty($config->get('refresh_token'))) {
      $form['status'] = [
        '#type' =>  'item',
        '#title' => $this->t('Status'),
        '#markup' => $this->t('Connection could not be established. Check your access token.'),
        '#weight' => 100,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mediaflow.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('refresh_token', $form_state->getValue('refresh_token'))
      ->set('allow_crop', $form_state->getValue('allow_crop'))
      ->set('set_alt_text', $form_state->getValue('set_alt_text'))
      ->set('ckeditor_type', $form_state->getValue('ckeditor_type'))
      ->set('method', $form_state->getValue('method'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  public function mediaTypeOptions() {
    $options = [];
    /** @var MediaType $media_type */
    foreach (MediaType::loadMultiple() as $media_type) {
      $source = $media_type->getSource();
      if ($source instanceof Mediaflow) {
        $options[$media_type->id()] = $media_type->label();
      }
    }
    return $options;
  }

  private function methodOptions() {
    return array_map('t', self::METHODS);
  }

}
