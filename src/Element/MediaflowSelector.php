<?php


namespace Drupal\mediaflow\Element;


use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement as FormElementBase;
use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Component\Serialization\Json;
use Drupal\mediaflow\Service\MediaflowFetcher;

/**
 * Provides a media flow resource selector form element.
 *
 * Usage example:
 * @code
 * $build = [
 *   '#type' => 'mediaflow_selector',
 * ];
 * @endcode
 *
 * @FormElement("mediaflow_selector")
 */
class MediaflowSelector extends FormElementBase {

  /**
   * @return array
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#process' => [
        [$class, 'processMediaflowSelector'],
      ],
      '#element_validate' => [
        [$class, 'validateMediaflowSelector'],
      ],
    ];
  }

  /**
   * @param array $element
   * @param FormStateInterface $form_state
   * @param array $complete_form
   */
  public static function processMediaflowSelector(array &$element, FormStateInterface $form_state, array &$complete_form) {

    $settings = Drupal::config('mediaflow.settings');

    $element['#tree'] = TRUE;

    $element['value'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'data-mediaflow-target' => TRUE,
      ],
    ];

    $element['filename'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $element['#value'],
      '#attributes' => [
        'data-mediaflow-display' => TRUE,
      ],
    ];

    $limit_file_type = 'image,video,srt,vtt';
    if (!empty($element['#media_type'])) {
      $limit_file_type = $element['#media_type'];
    }

    $api_settings = [
      'client_id'=> MediaflowFetcher::CLIENT_ID,
      'client_secret'=> MediaflowFetcher::CLIENT_SECRET,
      'refresh_token'=> $settings->get('refresh_token'),
      'allowSelectFormat' => $settings->get('allow_select_format'),
      'setAltText' => $settings->get('set_alt_text'),
      'locale' => Drupal::currentUser()->getPreferredAdminLangcode() != 'sv' ? 'en_US' : 'sv_SE',
      'limitFileType' => $limit_file_type,
    ];

    $element['mediaflowbrowser'] = [
     '#type' => 'markup',
     '#markup' => '<div id="mediaflow" style="position:relative;width:1000px;height:500px;overflow:hidden;border:1px solid black;margin:10px;" data-mediaflow-selector></div>',
     '#attached' => [
       'library' => ['mediaflow/element.selector'],
       'drupalSettings' => ['mediaflow' => $api_settings],
     ],
   ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      $element += ['#default_value' => ''];
      return $element['#default_value'];
    }
    if (isset($input['value'])) {
      $data = Json::decode($input['value']);
      return Drupal::service('mediaflow.usage_manager')->addMedia($data);
    }
    return [];
  }

  public static function validateMediaflowSelector(&$element, FormStateInterface $form_state, &$complete_form) {
    $input = $form_state->getValue($element['#parents']);
    if (isset($input['value'])) {
      $data = Json::decode($input['value']);
      $mediaflow_id = Drupal::service('mediaflow.usage_manager')->addMedia($data);
      if (!empty($mediaflow_id)) {
        $form_state->setValueForElement($element, $mediaflow_id);
      }
      else {
        // @todo Error message
      }
    }
  }

}
