<?php


namespace Drupal\mediaflow\Controller;


use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\media\MediaStorage;
use Drupal\mediaflow\Service\UsageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class MediaflowController implements ContainerInjectionInterface {

  /**
   * @var \Drupal\mediaflow\Service\UsageManager
   */
  private $usageManager;

  /**
   * @var \Drupal\media\MediaStorage
   */
  private $mediaStorage;

  /**
   * Constructs an MediaFilterController instance.
   *
   * @param \Drupal\mediaflow\Service\UsageManager $usage_manager
   * @param \Drupal\media\MediaStorage $media_storage
   *   The media storage.
   */
  public function __construct(UsageManager $usage_manager, MediaStorage $media_storage) {
    $this->usageManager = $usage_manager;
    $this->mediaStorage = $media_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mediaflow.usage_manager'),
      $container->get('entity_type.manager')->getStorage('media'),
    );
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addMedia() {
    $request = Drupal::request();
    $data = Json::decode($request->get('mediaflow-info'));

    $media_type_id = $request->get('media-type');
    $mediaflow_id = $this->usageManager->addMedia($data);

    /** @var MediaType $media_type */
    $media_type = MediaType::load($media_type_id);
    $field_name = $media_type->getSource()->getSourceFieldDefinition($media_type)->getName();
    /** @var \Drupal\media\Entity\Media $media */
    $media = $this->mediaStorage->create([
      'bundle' => $media_type->id(),
      $field_name => $mediaflow_id,
    ]);
    $media->setName($data['filename']);
    $media->save();

    $createdMedia = Media::load($media->id());

    $fid = $createdMedia->getSource()->getSourceFieldValue($createdMedia);
    $file = File::load($fid);
    $url = !empty($file) ? $file->createFileUrl() : "";
    
    return new JsonResponse([
      'src' => $url,
      'data-entity-uuid' => $media->uuid(),
      'data-entity-type' => 'media',
    ]);
  }

  public function embeddedMedia($id) {
    return new JsonResponse([
      'markup' => $this->usageManager->getValue($id, 'html'),
    ]);
  }
}
