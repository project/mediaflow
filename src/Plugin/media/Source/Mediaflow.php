<?php

namespace Drupal\mediaflow\Plugin\media\Source;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\media\Annotation\MediaSource;
use Drupal\media\IFrameUrlHelper;
use Drupal\media\MediaSourceInterface;
use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolverInterface;
use Drupal\mediaflow\Service\UsageManager;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media source plugin for Mediaflow Video.
 *
 * @MediaSource(
 *   id = "mediaflow",
 *   label = @Translation("Mediaflow"),
 *   description = @Translation("Embedded Mediaflow Video."),
 *   allowed_field_types = {"integer"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   providers = {},
 * )
 */
class Mediaflow extends MediaSourceBase implements MediaSourceInterface {

  /**
   * @var \Drupal\mediaflow\Service\UsageManager
   */
  protected $usageManager;

  /**
   * Constructs a new OEmbed instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, FieldTypePluginManagerInterface $field_type_manager, UsageManager $usage_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->usageManager = $usage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('mediaflow.usage_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'string' => [],
    ];
  }

  public function getMetadataAttributes() {
    return [
      'name' => $this->t('Mediaflow resource name'),
      'html' => $this->t('The code to embed'),
      'url' => $this->t('The url of the resource'),
      'id' => $this->t('The Mediaflow instance id'),
      'mediaId' => $this->t('The Mediaflow resource id'),
      'filetype' => $this->t('File type'),
      'width' => $this->t('Width'),
      'height' => $this->t('Photographer'),
    ];
  }

  /**
   * @param \Drupal\media\MediaInterface $media
   * @param string $attribute_name
   *
   * @return mixed|string|void|null
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $source = $media->getSource();
    if ($source instanceof Mediaflow) {
      $id = $source->getSourceFieldValue($media);
      if ($attribute_name == 'thumbnail_uri') {
        return $this->getThumbnailUri($id);
      }
      return $this->usageManager->getValue($id, $attribute_name);
    }
    return FALSE;
  }

  public function getThumbnailUri($id) {
    $type =  $this->usageManager->getValue($id, 'type');
    if ($type == UsageManager::FILE) {
      $fid = $this->usageManager->getValue($id, 'file');
      if ($fid) {
        /** @var File $file */
        $file = File::load($fid);
        return $file->getFileUri();
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'mediaflow',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);
    $source_field = $this->getSourceFieldDefinition($type)->getName();
    $display->setComponent($source_field, [
      'type' => 'mediaflow_integer',
      'weight' => $display->getComponent($source_field)['weight'],
    ]);
    $display->removeComponent('name');
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $plugin_definition = $this->getPluginDefinition();
    $label = (string) $this->t('@type media', [
      '@type' => $plugin_definition['label'],
    ]);
    return parent::createSourceField($type)->set('label', $label);
  }
}
