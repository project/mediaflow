<?php

namespace Drupal\mediaflow\Plugin\EntityBrowser\Widget;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\entity_browser\Annotation\EntityBrowserWidget;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\file\Entity\File;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\Entity\MediaType;
use Drupal\mediaflow\Plugin\media\Source\Mediaflow as MediaflowSource;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Integration with Mediaflow library.
 *
 * @EntityBrowserWidget(
 *   id = "mediaflow",
 *   label = @Translation("Mediaflow"),
 *   description = @Translation("Integrates with Mediaflow")
 * )
 */
class Mediaflow extends WidgetBase {

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
      return parent::defaultConfiguration();
  }

  /**
   * Constructs a new View object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EventDispatcherInterface $event_dispatcher,
    EntityTypeManagerInterface $entity_type_manager,
    WidgetValidationManager $validation_manager,
    AccountProxyInterface $current_user
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $event_dispatcher,
      $entity_type_manager,
      $validation_manager
    );
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    $form['mediaflow'] = [
      '#type' => 'mediaflow_selector',
    ];
    return $form;
  }

  /**
   * Returns the media bundle that this widget creates.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   Media bundle.
   */
  protected function getBundle() {
    if (!empty($this->configuration['bundle'])) {
      $type = $this->configuration['bundle'];
    }
    else {
      $type = 'mediaflow';
    }
    return $this->entityTypeManager
      ->getStorage('media_type')
      ->load($type);
  }

  /**
   * Prepares the entities without saving them.
   *
   * We need this method when we want to validate or perform other operations
   * before submit.
   *
   * @param array $form
   *   Complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities.
   */
  protected function prepareEntities(
  array $form, FormStateInterface $form_state
  ) {
      return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('mediaflow')) {
          parent::validate($form, $form_state);
      } else {
          $form_state->setErrorByName('mediaflow', $this->t('You have to choose media asset before'));
      }
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('mediaflow');
    $media_type = $this->getBundle();
    $source_plugin = $media_type->getSource();
    if ($source_plugin instanceof MediaflowSource) {
      $field_name = $source_plugin->getSourceFieldDefinition($media_type)->getName();
      /** @var \Drupal\media\Entity\Media $media */
      $storage = $this->entityTypeManager->getStorage('media');
      $media = $storage->create([
        'bundle' => $media_type->id(),
        $field_name => $value,
      ]);
      $media->save();
      $this->selectEntities([$media], $form_state);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function access() {
    if ($this->currentUser->hasPermission('access mediaflow widget')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $bundles = [];
    /** @var MediaType $media_type */
    foreach (MediaType::loadMultiple() as $media_type) {
      if ($media_type->getSource() instanceof MediaflowSource) {
        $bundles[$media_type->id()] = $media_type->label();
      }
    }

    $form['bundle'] = [
      '#type' => 'container',
      'select' => [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#options' => $bundles,
        '#default_value' => $this->getBundle()->id(),
      ],
      '#attributes' => ['id' => 'bundle-wrapper-' . $this->uuid()],
    ];

    $form['info'] = [
      '#markup' => '<em>' . $this->t('Tip: Use larger dimensions for the modal display plugin to fit all elements of the mediaflow browser.') . '</em>'
    ];

    return $form;
  }

  public function isConfigurable() {
    return TRUE;
  }

}
