<?php

namespace Drupal\mediaflow\Plugin\CKEditorPlugin;

use Drupal;
use Drupal\ckeditor\Annotation\CKEditorPlugin;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mediaflow\Service\MediaflowFetcher;

/**
 * Defines the "mediaflow" ckeditor plugin.
 *
 * @CKEditorPlugin(
 *   id = "mediaflow",
 *   label = @Translation("Embed media from Mediaflow"),
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class Mediaflow extends CKEditorPluginBase implements ContainerFactoryPluginInterface, CKEditorPluginCssInterface {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The media type entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $mediaTypeStorage;

  /**
   * Constructs a new DrupalMediaLibrary plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleExtensionList $extension_list_module, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleExtensionList = $extension_list_module;
    $this->mediaTypeStorage = $entity_type_manager->getStorage('media_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [
      'drupalmedia',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'editor/drupal.editor.dialog',
      'mediaflow/jsapi'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->moduleExtensionList->getPath('mediaflow') . '/js/mediaflow.ckeditor.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $settings = Drupal::config('mediaflow.settings')->getRawData();
    $settings['client_id'] = MediaflowFetcher::CLIENT_ID;
    $settings['client_secret'] = MediaflowFetcher::CLIENT_SECRET;
    return ['mediaflow' => $settings];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Mediaflow' => [
        'label' => $this->t('Insert from Mediaflow'),
        'image' => $this->moduleExtensionList->getPath('mediaflow') . '/js/icons/mediaflow.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return [
      $this->moduleExtensionList->getPath('mediaflow') . '/css/plugins/mediaflow/ckeditor.mediaflow.css',
    ];
  }
}
