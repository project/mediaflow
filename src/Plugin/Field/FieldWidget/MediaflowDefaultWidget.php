<?php

namespace Drupal\mediaflow\Plugin\Field\FieldWidget;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleStorageInterface;
use Drupal\media\Entity\MediaType;
use Drupal\mediaflow\Plugin\media\Source\Mediaflow;
use Drupal\mediaflow\Service\UsageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field widget "mediaflow_default".
 *
 * @FieldWidget(
 *   id = "mediaflow_default",
 *   label = @Translation("Mediaflow"),
 *   field_types = {
 *     "mediaflow_item",
 *   }
 * )
 */
class MediaflowDefaultWidget extends StringTextfieldWidget {

  const METHODS = [
    'default' => 'Use site setting',
    'choose' => 'Choose for each resource',
    'iframe' => 'Iframe',
    'js' => 'Javascript',
  ];

  const MEDIA_TYPES = [
    'image,video' => 'Both images and video',
    'image' => 'Images',
    'video' => 'Video',
  ];

  /**
   * The mediaflow settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\mediaflow\Service\UsageManager
   */
  protected $usageManager;

  /**
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ImageStyleStorageInterface $image_style_storage,
    ConfigFactoryInterface $config_factory,
    UsageManager $usage_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->config = $config_factory->get('mediaflow.settings');
    $this->usageManager = $usage_manager;
    $this->imageStyleStorage = $image_style_storage;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('config.factory'),
      $container->get('mediaflow.usage_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'method' => 'default',
        'media_type' => 'image,video',
      ] + parent::defaultSettings();
  }
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    /*$form['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Allowed media types'),
      '#default_value' => $this->getSetting('media_type'),
      '#options' => $this->mediaTypeOptions(),
    ];
    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Embed method for video.'),
      '#default_value' => $this->getSetting('method'),
      '#options' => $this->methodOptions(),
    ];*/
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    /*$summary[] = t('Allowed media: @media_type', [
      '@media_type' => $this->mediaTypeOptions()[$this->getSetting('media_type')]
    ]);
    if ($this->getSetting('media_type') !== 'image') {
      $summary[] = t('Embed method: @method', [
        '@method' => $this->methodOptions()[$this->getSetting('method')]
      ]);
    }*/
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element;
    $value = $items[$delta]->value;

    if (!empty($value)) {
      $method = $this->getSetting('method');
      if ($method === 'default') {
        $method = $this->config->get('method');
      }
      $element['preview'] = $this->viewElement($items, $delta, 'en');

      $element['value']['#value'] = $value;
      $element['value']['#media_type'] = $this->getSetting('media_type');
      $element['value']['#method'] = $method;
      $element['value']['#type'] = 'mediaflow_selector_file';
      $element['value']['#prefix'] = '<div class="mediaflow-widget-wrapper-file">';
      $element['value']['#suffix'] = '</div>';
    }
    else {
      $method = $this->getSetting('method');
      if ($method === 'default') {
        $method = $this->config->get('method');
      }
      $element['value']['#prefix'] = '<div class="mediaflow-widget-wrapper-file">';
      $element['value']['#suffix'] = '</div>';
      $element['value']['#type'] = 'mediaflow_selector_file';
      $element['value']['#media_type'] = $this->getSetting('media_type');
      $element['value']['#method'] = $method;
    }
    return $element;
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $langcode
   *
   * @return array
   */
  public function viewElement(FieldItemListInterface $items, $delta, $langcode) {
    $value = $items->offsetGet($delta)->getValue();

    $type = $this->usageManager->getValue($value, 'type');
    if ($type == UsageManager::EMBED) {
      return [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'data-mediaflow-id' => $value,
          'class' => [
            'mediaflow-video-placeholder'
          ]
        ],
        '#attached' => [
          'library' => ['mediaflow/embedd_video']
        ],
      ];
    }
    if ($type == UsageManager::FILE) {
      $fid = $this->usageManager->getValue($value, 'file');
      /** @var FileInterface $file */
      $file = File::load($fid);
      if (!empty($file)) {
        return [
          '#theme' => 'image_style',
          '#uri' => $file->getFileUri(),
          '#style_name' => 'large',
        ];
      }
    }
    return [
      '#markup' => t('Malformed Mediaflow Entity'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewImage(FieldItemInterface $item, FileInterface $file) {
    return [
      '#theme' => 'image_formatter',
      '#item' => $item,
      //'#image_style' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
 /* public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_bundle = $field_definition->getTargetBundle();

    if (!parent::isApplicable($field_definition) || $field_definition->getTargetEntityTypeId() !== 'media' || !$target_bundle) {
      return FALSE;
    }
    return MediaType::load($target_bundle)->getSource() instanceof Mediaflow;
  }*/

  /*private function methodOptions() {
    return array_map('t', self::METHODS);
  }

  private function mediaTypeOptions() {
    return array_map('t', self::MEDIA_TYPES);
  }

  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    return parent::extractFormValues($items, $form, $form_state); // TODO: Change the autogenerated stub
  }*/

}
