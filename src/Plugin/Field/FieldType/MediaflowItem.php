<?php
namespace Drupal\mediaflow\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * Provides a field for Mediaflow plugin
 * 
 * @FieldType(
 *   id = "mediaflow_item",
 *   label = @Translation("Mediaflow"),
 *   description = @Translation("Mediaflow"),
 *   category = @Translation("Reference"),
 *   default_widget = "mediaflow_default",
 *   default_formatter = "mediaflow_default",
 * )
 */

class MediaflowItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $output = array();
    $output['columns']['value'] = array(
      'type' => 'varchar',
      'length' => 255
    );

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Media'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    $item = $this->getValue();

    $has_stuff = FALSE;

    if (isset($item['value']) && !empty($item['value'])) {
      $has_stuff = TRUE;
    }

    return !$has_stuff;

  }
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      ] + parent::defaultSettings();
  }
}