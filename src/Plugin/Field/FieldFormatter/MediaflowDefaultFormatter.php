<?php

namespace Drupal\mediaflow\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\media\Entity\MediaType;
use Drupal\mediaflow\Plugin\media\Source\Mediaflow;
use Drupal\mediaflow\Service\UsageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field formatter "mediaflow_default".
 *
 * @FieldFormatter(
 *   id = "mediaflow_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "mediaflow_item",
 *   }
 * )
 */
class MediaflowDefaultFormatter extends FormatterBase {

  /**
   * @var \Drupal\mediaflow\Service\UsageManager
   */
  protected $usageManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\mediaflow\Service\UsageManager $mediaflow_usage_manager
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    AccountInterface $current_user,
    EntityStorageInterface $image_style_storage,
    UsageManager $mediaflow_usage_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->usageManager = $mediaflow_usage_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('mediaflow.usage_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'image_style' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );

    $element['image_style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = array();

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};
      if (empty($value)) {
        continue;
      }
      $method = $this->usageManager->getValue($value, 'type');
      if ($method == UsageManager::EMBED) {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->usageManager->getValue($value, 'filename'),
          '#attributes' => [
            'data-mediaflow-id' => $value,
            'class' => [
              'mediaflow-video-placeholder'
            ]
          ],
          '#attached' => [
            'library' => ['mediaflow/embedd_video']
          ],
        ];
      }
      if ($method == UsageManager::FILE) {
        /** @var FileInterface $file */
        $file = File::load($this->usageManager->getValue($value, 'file'));
        if (!empty($file)) {
          if ($this->getSetting('image_style')) {
            $element[$delta] = [
              '#theme' => 'image_style',
              '#uri' => $file->getFileUri(),
              '#style_name' => $this->getSetting('image_style'),
            ];
          }
          else {
            $element[$delta] = [
              '#theme' => 'image',
              '#uri' => $file->getFileUri(),
            ];
          }

        }
      }
    }
    return $element;
  }

}
