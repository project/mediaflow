<?php


namespace Drupal\mediaflow\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\media\MediaInterface;
use Drupal\mediaflow\Plugin\media\Source\Mediaflow;
use Drupal\node\NodeInterface;
use Exception;

class UsageManager extends ServiceProviderBase {

  const FILE = 1;
  const EMBED = 2;

  const REFERENCE_FIELD_TYPES = [
    'entity_reference',
    'image',
    'entity_reference_revisions',
  ];

  const MEDIA_EMBED_FIELD_TYPES = [
    'text',
    'text_long',
    'text_with_summary',
  ];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\mediaflow\Service\MediaflowFetcher
   */
  private $fetcher;

  /**
   * Base Database API class.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  private $entityFieldManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  private $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\mediaflow\Service\MediaflowFetcher $fetcher
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   */
  public function __construct(
    EntityFieldManager $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    MediaflowFetcher $fetcher,
    ModuleHandler $module_handler
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->fetcher = $fetcher;
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * @param $data
   */
  public function downloadUri(&$data) {
    if (empty($data['mediaType']) || $data['mediaType'] !== 'video') {
      $data['file'] = $this->fetcher->downloadFile($data);
    }
  }

  /**
   * @param $data
   *
   * @return \Drupal\Core\Database\StatementInterface|false|int|string|null
   */
  public function addMedia($data) {

    if (isset($data['embedCode'])) {
      $values = [
        'mediaflow_origin_id' => $data['id'],
        'data' => serialize($data),
        'html' => $data['embedCode'],
        'type' => self::EMBED,
      ];

      // @todo If the fileselector starts returning a thumbnail url you
      // could add this here.

      // $data['url'] => $data['thumbnail'];
      // $this->downloadUri($data);
      // $value['file'] = $data['file'];
    }
    else if (!empty($data['url'] && empty($data['file']))) {
      $this->downloadUri($data);
      if (isset($data['file'])) {
        $values = [
          'mediaflow_origin_id' => $data['id'],
          'data' => serialize($data),
          'file' => $data['file']->id(),
          'type' => self::FILE,
        ];
      }
    }

    if (!empty($values)) {
      try {
        return $this->database->insert('mediaflow')
        ->fields($values)
        ->execute();
      } catch (Exception $exception) {
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * @param $id
   * @param $key
   *
   * @return false|mixed
   */
  public function getValue($id, $key) {
    if ($this->database->schema()->fieldExists('mediaflow', $key)) {
      return $this->database->select('mediaflow', 'fm')
        ->fields('fm', [$key])
        ->condition('fm.id', $id)
        ->execute()->fetchField();
    }
    else {
      $data = unserialize($this->getValue($id, 'data'));
      if (isset($data[$key])) {
        return $data[$key];
      }
    }
    return FALSE;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function onEntityUpdate(EntityInterface $entity) {
    /** @var NodeInterface[] $nodes */
    /** @var MediaInterface[] $medias */
    [$nodes, $medias] = $this->discoverEntities($entity);
    foreach ($nodes as $node) {
      foreach ($medias as $media) {
        $source = $media->getSource();
        if ($source instanceof Mediaflow) {
          $ids[] = $source->getSourceFieldValue($media);
        }
      }
      if (empty($ids)) {
        continue;
      }
      $mf_ids = $this->database->select('mediaflow', 'fm')
        ->fields('fm', ['mediaflow_origin_id'])
        ->condition('fm.id', $ids, 'IN')
        ->execute()->fetchCol();
      $url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
      if (count($mf_ids)) {
         $data = [
           'id' => $mf_ids,
           'contact' => $node->getOwner()->label(),
           'date' => $node->getRevisionCreationTime(),
           'amount' => '1',
           'web' => [
            'page' => $url,
            'pageName' => $node->label()
           ],
           'project' => $url,
           'types' => ['web'],
         ];      
        
        $jsonResponse = $this->fetcher->updateUsages($data);
      }
    }
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function onEntityDelete(EntityInterface $entity) {
    /** @var NodeInterface[] $nodes */
    /** @var MediaInterface[] $medias */
    [$nodes, $medias] = $this->discoverEntities($entity);
    foreach ($nodes as $node) {
      foreach ($medias as $media) {
        $source = $media->getSource();
        if ($source instanceof Mediaflow) {
          $ids[] = $source->getSourceFieldValue($media);
        }
      }
      if (empty($ids)) {
        continue;
      }
      $mf_ids = $this->database->select('mediaflow', 'fm')
        ->fields('fm', ['mediaflow_origin_id'])
        ->condition('fm.id', $ids, 'IN')
        ->execute()->fetchCol();
      $url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
      if (count($mf_ids)) {
        $data = [
          'id' => $mf_ids,
          'types' => ['web'],
          'amount' => '1',
          'web' => [
            'page' => $url,
            'pageName' => $node->label()
           ],
          'project' => $url,
          'removed' => TRUE 
        ];
        $this->fetcher->deleteUsage($data);
      }
    }
  }

  /**
   * Takes an entity being altered and discovers what files and what nodes
   * are affected by the change.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  private function discoverEntities(EntityInterface $entity) {
    $medias = [];
    $nodes = [];
    if ($entity instanceof NodeInterface) {
      $medias = $this->mediaInNode($entity);
      $nodes = [$entity];
    }
    if ($entity instanceof MediaInterface) {
      $medias = [$entity];
      $nodes = $this->nodesContainingMedia($entity);
    }
    return [$nodes, $medias];
  }

  /**
   * Finds any files nested in this node.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function mediaInNode(NodeInterface $node) {
    $media = [];
    if ($this->moduleHandler->moduleExists('paragraphs')) {
      $paragraphs = $this->getReferencedEntities($node, 'paragraph');
      foreach ($paragraphs as $paragraph) {
        array_push($media, ...$this->getReferencedEntities($paragraph, 'media'));
        array_push($media, ...$this->getEmbeddedMediaEntities($paragraph));
      }
    }
    array_push($media, ...$this->getReferencedEntities($node, 'media'));
    array_push($media, ...$this->getEmbeddedMediaEntities($node));
    return $media;
  }

  /**
   * Finds any nodes containing given media.
   *
   * @param \Drupal\media\MediaInterface $media
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function nodesContainingMedia(MediaInterface $media) {
    $nodes = [];
    if ($this->moduleHandler->moduleExists('paragraphs')) {
      $paragraphs = $this->getEntitiesWithEmbededMedia($media, 'paragraph');
      array_push($paragraphs, ...$this->getReferencingEntities($media,'paragraph'));
      foreach ($paragraphs as $paragraph) {
        array_push($nodes, ...$this->getReferencingEntities($paragraph, 'node'));
      }
    }
    array_push($nodes, ...$this->getEntitiesWithEmbededMedia($media, 'node'));
    array_push($nodes, ...$this->getReferencingEntities($media,'node'));
    return $nodes;
  }

  /**
   * Finds any entities containing this entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $entity_type
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getReferencingEntities(EntityInterface $entity, $entity_type) {
    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface[] $fieldDefinitions */
    $fieldDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
    $reference_fields = array_filter($fieldDefinitions, function ($definition) use ($entity) {
      return in_array($definition->getType(), self::REFERENCE_FIELD_TYPES)
        && $definition->getSetting('target_type') == $entity->getEntityTypeId();
    });
    if (!count($reference_fields)) {
      return [];
    }
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $query = $storage->getQuery('OR');
    foreach ($reference_fields as $definition) {
      $query->condition($definition->getName(), $entity->id());
    }
    $ids = $query->execute();
    if (!count($ids)) {
      return [];
    }
    return $storage->loadMultiple($ids);
  }

  /**
   * Finds any entities with this media embedded in it's wysiwyg fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $entity_type
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEntitiesWithEmbededMedia(EntityInterface $entity, $entity_type) {
    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface[] $fieldDefinitions */
    $fieldDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
    $reference_fields = array_filter($fieldDefinitions, function ($definition) use ($entity) {
      return in_array($definition->getType(), self::MEDIA_EMBED_FIELD_TYPES);
    });
    if (!count($reference_fields)) {
      return [];
    }
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $query = $storage->getQuery('OR');
    foreach ($reference_fields as $definition) {
      $media_string = 'data-entity-uuid="' . $entity->uuid() . '"';
      $query->condition($definition->getName(), $media_string, 'CONTAINS');
    }
    $ids = $query->execute();
    if (!count($ids)) {
      return [];
    }
    return $storage->loadMultiple($ids);
  }

  /**
   * Finds any entities nested in this entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   * @param $entity_type
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   */
  private function getReferencedEntities(FieldableEntityInterface $entity, $entity_type) {
    $reference_fields = array_filter($entity->getFieldDefinitions(), function ($definition) use ($entity_type) {
      return in_array($definition->getType(), self::REFERENCE_FIELD_TYPES)
        && $definition->getSetting('target_type') == $entity_type;
    });
    if (!count($reference_fields)) {
      return [];
    }

    $targets = [];
    foreach ($reference_fields as $definition) {
      $name = $definition->getName();
      if ($entity->hasField($name)) {
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field_item */
        $field_item = $entity->get($name);
        array_push($targets, ...$field_item->referencedEntities());
      }
    }
    return $targets;
  }

  /**
   * Finds any media embedded in wysiwyg fields in this entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEmbeddedMediaEntities(FieldableEntityInterface $entity) {
    // Find field that may have embedded media.
    $fields = array_filter($entity->getFieldDefinitions(), function ($definition) {
      return in_array($definition->getType(), self::MEDIA_EMBED_FIELD_TYPES);
    });
    if (!count($fields)) {
      return [];
    }

    // Use regular expressions to search for embedded media.
    $uuids = [];
    foreach ($fields as $definition) {
      $name = $definition->getName();
      if ($entity->hasField($name)) {
        $field_item = $entity->get($name);
        $texts = array_column($field_item->getValue(), 'value');
        $text = implode($texts);
        $matches = [];
        preg_match_all('/<drupal-media.*data-entity-uuid="(.+)"/', $text,$matches);
        foreach ($matches[1] as $match) {
          $uuids[] = $match;
        }
      }
    }

    // Load and return any found media entities.
    if (!count($uuids)) {
      return [];
    }
    $storage = $this->entityTypeManager->getStorage('media');
    $query = $storage->getQuery();
    $query->condition('uuid', $uuids, 'IN');
    $ids = $query->execute();
    if (!count($ids)) {
      return [];
    }
    return $storage->loadMultiple($ids);
  }
}
