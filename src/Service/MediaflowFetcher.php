<?php


namespace Drupal\mediaflow\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;


class MediaflowFetcher extends ServiceProviderBase {

  const API_URL = 'https://customerapi.mediaflowpro.com/1';
  const CLIENT_ID = 'cZsQAAyL';
  const CLIENT_SECRET = 'ZdtUw5gvgGtnHjk8SB6qLM8kQzfbwM';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  private $settings;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\Client $client
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   */
  public function __construct(
    Client $client,
    ConfigFactory $config_factory,
    FileSystemInterface $file_system
  ) {
    $this->client = $client;
    $this->settings = $config_factory->getEditable('mediaflow.settings');
    $this->fileSystem = $file_system;

  }

  /**
   * @param $data

   * @return \Drupal\file\Entity\File|false
   */
  public function downloadFile($data) {
    $file = NULL;
    $client = new Client();
    $response = $client->get($data['url'], ['verify' => false]);

    if ($response->getStatusCode() == 200) {
      $ext = $data['filetype'];
      $filename = $data['name'] . '_' . date('Y-m-d--H-i-s') . '.' . $ext;
      if ($filename) {
        $file_data = $response->getBody()->getContents();
        $directory = 'public://mediaflow/';
        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
        $file = \Drupal::service('file.repository')->writeData($file_data, $directory . $filename, FileSystemInterface::EXISTS_REPLACE);
      }
    }
    return $file;
  }

  public function updateUsages($data) {
    $this->ensureAccess();
    $this->post('/files/multiple/usages', ['id' => implode(',', $data['id']), 'access_token' => $this->settings->get('access_token')], $data);
  }

  public function deleteUsage($data) {
    $this->ensureAccess();
    $this->post('/files/multiple/usages', ['id' => implode(',', $data['id']), 'access_token' => $this->settings->get('access_token')], $data);
  }

  /**
   * Renews access tokens if necessary.
   */
  public function ensureAccess() {
    //if (empty($this->settings->get('access_token'))
    //  || $this->settings->get('access_token_expires') < time()) {
      $this->renewAccessToken();
   // }
  }

  /**
   * Renews access tokens.
   *
   * @return bool
   */
  public function renewAccessToken() {
    try {
      $response = $this->get('/oauth2/token', [
        'grant_type' => 'refresh_token',
        'client_id' => self::CLIENT_ID,
        'client_secret' => self::CLIENT_SECRET,
        'refresh_token' => $this->settings->get('refresh_token'),
      ]);
      if (!empty($response['access_token'])) {
        $this->settings->set('access_token', $response['access_token']);
        /*if (!empty($response['expires_in'])) {
          $this->settings->set('access_token_expires', time() + $response['expires_in']);
        }*/
        $this->settings->save();
        return TRUE;
      }
    } catch (ClientException $exception) {
    }
    return FALSE;
  }

  /**
   * Performs a GET request to the api.
   *
   * @param $resource
   * @param array $query
   *
   * @return array
   */
  private function get($resource, $query = [], $body = []) {
    $headers = [];

    if (!empty($query['access_token'])) {
      $headers['Authorization'] = 'Bearer ' . $query['access_token'];
      unset($query['access_token']);
    }
    $response = $this->client->get(self::API_URL . $resource, [
      'query' => $query,
      'headers' => $headers,
    ]);
    return Json::decode($response->getBody());
  }

  /**
   * Performs a POST request to the api.
   *
   * @param $resource
   * @param array $query
   *
   * @return array
   */
  private function post($resource, $query = [], $body = []) {
    $headers = [];
    if (!empty($query['access_token'])) {
      $headers['Authorization'] = 'Bearer ' . $query['access_token'];
      //unset($query['access_token']);
      unset($body['access_token']);
    }
    $headers['Content-Type'] = 'application/json';
    $response = $this->client->post(self::API_URL . $resource, [
      'query' => $query,
      'body' => json_encode($body),
      'headers' => $headers,
    ]);
    
    return Json::decode($response->getBody());
  }
}
